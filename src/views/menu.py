from pglet import Stack
from pglet import Textbox
from pglet import Button
from pglet import Checkbox
from pglet import Event
from pglet import Page
from pglet import button


class Menu:
    def __init__(self, page:Page) -> None:
        self.__page = page

        self.view_stack = Stack()

        self.menu_button = Button(
            text="Menu", 
            menu_items=[
                button.MenuItem(text='Email message', icon='Mail'),
                button.MenuItem(text='Calendar event', icon='Calendar')
            ]
        )

        self.view = Stack(
            vertical_align="start",
            controls=[
                self.view_stack,
                self.menu_button
            ]
        )
