import pglet

from app import *


def main(page:pglet.Page):
    page.title = "Registry UI"
    page.horizontal_align = "center"
    page.height = "100%"
    page.theme = APP_DEFAULT_THEME
    page.update()

    app = App(page)

    page.add(
        app.head,
        app.view
    )


pglet.app("Registry UI", target=main)
